# statusbar-audio

A simple program that prints out volume status information whenever a volume event happens. Useful for statusbars like `lemonbar`. It works with pulseaudio and pipewire-pulse.

## Usage

`statusbar-audio` is flexible.  Just running the program outputs a stream of text like:
```
head 25%
head 25%
head 25%
head 25%
speak 20%
speak 20%
```
The available commands brings more out of it. You should be able to set it up with colors and icons using these:
```
COMMANDS:

prepend:           Add text to beginning
prepend-mute:      Add text to beginning when mute (replaces prepend-not-mute)
prepend-not-mute:  Add text to beginning when not mute (replaces prepend-mute)
icon-head:         Icon for headphones
icon-speaker:      Icon for speakers
icon-unknown:      Icon for unknown port
append:            Add text to the end
```
An example for `lemonbar`: `statusbar-audio prepend-mute '%{F#cba6f7}mute ' append '%%{F-}'`. This adds text that says "mute" and a different color whenever audio is muted.

## Installation

- `sudo ./install`
- Note the `install_dir` in the script. You might have to change that depending on your system.

### Uninstall

- `sudo ./install uninstall`

## Dependencies

- Relies on https://github.com/mafik/pulseaudio
