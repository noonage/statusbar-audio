package main

import (
	"fmt"
	"log"
	"math"
	"os"

	"github.com/mafik/pulseaudio"
)

func help() {
	help := "" +
		"statusbar-audio" + "\n" +
		"Get statusbar information for audio level/port/mute status" + "\n" +
		"\nCOMMANDS:\n\n" +
		"prepend:           Add text to beginning\n" +
		"prepend-mute:      Add text to beginning when mute (replaces prepend-not-mute)\n" +
		"prepend-not-mute:  Add text to beginning when not mute (replaces prepend-mute)\n" +
		"icon-head:         Icon for headphones\n" +
		"icon-speaker:      Icon for speakers\n" +
		"icon-unknown:      Icon for unknown port\n" +
		"append:            Add text to the end\n" +
		"help:              Print this help info\n"

	fmt.Println(help)
}

type volumeConfig struct {
	prepend        string
	append         string
	mutePrepend    string
	notMutePrepend string
	headIcon       string
	speakIcon      string
	unknownIcon    string
}

func printStatus(client *pulseaudio.Client, config *volumeConfig) {
	var outStr string

	outStr = config.prepend

	if m, err := client.Mute(); m {
		if err != nil {
			log.Panic()
		}

		outStr = outStr + config.mutePrepend
	} else {
		outStr = outStr + config.notMutePrepend
	}

	lvl, err := client.Volume()
	if err != nil {
		log.Panic()
	}

	out, idx, err := client.Outputs()
	if err != nil {
		log.Panic()
	}

	switch out[idx].PortName {
	case "Headphones":
		outStr = outStr + config.headIcon
	case "Speaker":
		outStr = outStr + config.speakIcon
	default:
		outStr = outStr + config.unknownIcon
	}

	os.Stdout.WriteString("" +
		outStr +
		fmt.Sprintf("%v%%", math.Round(float64(lvl*100))) +
		config.append + "\n")
}

func main() {
	var config volumeConfig

	config.headIcon = "head "
	config.speakIcon = "speak "
	config.unknownIcon = "? "

	args := os.Args[1:]
	ignore := false
	for idx, arg := range args {
		if ignore {
			ignore = false
			continue
		}

		switch arg {
		case "prepend":
			config.prepend = args[idx+1]
		case "prepend-mute":
			config.mutePrepend = args[idx+1]
		case "prepend-not-mute":
			config.notMutePrepend = args[idx+1]
		case "icon-head":
			config.headIcon = args[idx+1]
		case "icon-speaker":
			config.speakIcon = args[idx+1]
		case "icon-unknown":
			config.unknownIcon = args[idx+1]
		case "append":
			config.append = args[idx+1]
		case "help":
			help()
			os.Exit(1)
		default:
			log.Panicf("unknown argument: %s(see help for available commands!)", arg)
		}

		ignore = true
	}

	client, err := pulseaudio.NewClient()
	if err != nil {
		log.Panic(err)
	}
	defer client.Close()

	printStatus(client, &config)

	events, err := client.Updates()
	if err != nil {
		log.Panic(err)
	}

	for {
		if _, ok := <-events; ok {
			printStatus(client, &config)
		} else {
			break
		}
	}
}
